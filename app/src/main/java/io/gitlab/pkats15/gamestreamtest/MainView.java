package io.gitlab.pkats15.gamestreamtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceView;
import android.view.SurfaceHolder;


public class MainView extends SurfaceView implements SurfaceHolder.Callback {
    public MainView(Context context) {
        super(context);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas c = holder.lockCanvas();
        c.drawRGB(0, 0, 0);
        Paint paint = new Paint();
        paint.setARGB(255, 255, 0, 255);
        Paint paint2 = new Paint();
        paint2.setARGB(255, 0, 255, 0);
        c.drawLine(0, 0, 200, 200, paint);
        c.drawLine(200, 200, 400, 0, paint2);
        holder.unlockCanvasAndPost(c);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
