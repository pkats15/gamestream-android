package io.gitlab.pkats15.gamestreamtest;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import io.gitlab.pkats15.gamestreamtest.MainView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FrameLayout frame = (FrameLayout) findViewById(R.id.surfaceFrame);
        SurfaceView surface = new MainView(this);
        SurfaceHolder holder = surface.getHolder();
        holder.addCallback((SurfaceHolder.Callback) surface);
        frame.addView(surface);


    }

}
